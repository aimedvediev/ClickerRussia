package com.example.antonmedvediev.clickerrussia;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.String.valueOf;

public class GameActivity extends AppCompatActivity {
    private Button buttonClick, buttonUpdateShovel, buttonUpdateGloves, buttonBuffBonfire, buttonBuffVodka, buttonBuyWorker, buttonBuffForeman;
    private TextView txtCountMoney, txtCountMetrs, txtCostOfGloves, txtCostOfShovel, txtCostOfBonfire, txtCostOfVodka, txtCostOfWorker, txtCostOfForeman;
    private Chronometer chronometerAll;
    //update to click(shovel)
    private static double digClick = 0.01;
    private static int levelUpdateShovel = 0;
    private static double costUpdateShovel = 0.5;
    static final double UPDATE_SHOVEL_1 = 0.03;
    static final double UPDATE_SHOVEL_2 = 0.05;
    //update to click(gloves)
    private static double gloves = 1;
    private static int levelUpdateGloves = 0;
    private static double costUpdateGloves = 1;
    static final double UPDATE_GLOVES_1 = 1.2;
    static final double UPDATE_GLOVES_2 = 1.5;
    //buff bonfire
    CountDownTimer timeOfBonfire;
    TextView textTimerBonfire;
    TextView textMetrBonfire;
    private static int buffFromBonfie = 1;
    private static boolean isAHot = false;
    private double costBuffBonfire = 0.5;
    private long timeOfBonfireLeft;
    private static double hotMetrToDig = 0;
    private static int bonfireIsOnFire = 1;
    //buff vodka
    CountDownTimer timeOfDrunk;
    CountDownTimer timeOfHangover;
    private static boolean isDrunk;
    private static boolean isHangover;
    TextView textTimerVodka;
    private final long TIME_OF_DRUNK_LEFT_1 = 60000;
    private final long TIME_OF_DRUNK_LEFT_2 = 120000;
    private final long TIME_OF_HANGOVER_LEFT_1 = 30000;
    private final long TIME_OF_DRUNK_HANGOVER_2 = 120000;
    private final long TIME_OF_LIKE_DEAD = 120000;
    private long timeOfVodkaLeft = 0;
    private final double BUFF_FROM_VODKA_LOW = 1.3;
    private final double BUFF_FROM_VODKA_MAX = 1.9;
    private final double DEBUFF_FROM_HANGOVER_LOW = 0.9;
    private final double DEBUFF_FROM_HANGOWER_MAX = 0.8;
    private final double DEBUFF_LIKE_DEAD = 0.1;
    private static double buffVodka = 1;
    private double costBuffVodka = 2;
    private final int LIMIT_TO_VODKA = 2;
    private static int countVodka = 0;
    //buff worker
    private final double COST_WORKER = 1;
    private final double SALLARY_WORKER_PER_SECOND = -0.05;
    private final double WORKER_DO_PER_SECOND = 0.25;
    private final int LIMIT_OF_WORKERS = 5;
    private int countOfWorkers = 0;
    private double trueWorker = 0;
    private static ArrayList<String> workers;
    //buff foreman
    CountDownTimer timeOfWorkForeman;
    TextView textTimerForeman;
    private int foremanBuff = 0;
    private long timeOfFaremanLeft;
    private final int COST_OF_CALL_FOREMAN = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        txtCountMetrs = (TextView) findViewById(R.id.how_many_meters);
        txtCountMoney = (TextView) findViewById(R.id.how_much_money);
        chronometerAll = (Chronometer) findViewById(R.id.chronometer_all);
        textTimerBonfire = (TextView) findViewById(R.id.timer_buff_bonfire);
        textTimerBonfire.setVisibility(View.INVISIBLE);
        textMetrBonfire = (TextView) findViewById(R.id.metr_buff_bonfire);
        textMetrBonfire.setVisibility(View.INVISIBLE);
        textTimerVodka = (TextView) findViewById(R.id.timer_buff_vodka);
        textTimerVodka.setVisibility(View.INVISIBLE);
        textTimerForeman = (TextView) findViewById(R.id.timer_buff_foreman);
        textTimerForeman.setVisibility(View.INVISIBLE);
        txtCostOfBonfire = (TextView) findViewById(R.id.cost_of_bonfire);
        txtCostOfForeman = (TextView) findViewById(R.id.cost_of_foreman);
        txtCostOfGloves = (TextView) findViewById(R.id.cost_of_gloves);
        txtCostOfShovel = (TextView) findViewById(R.id.cost_of_shovel);
        txtCostOfVodka = (TextView) findViewById(R.id.cost_of_Vodka);
        txtCostOfWorker = (TextView) findViewById(R.id.cost_of_worker);
        chronometerAll.start();
        workers = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));

        addListenerOnButton();

        chronometerAll.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costUpdateShovel && levelUpdateShovel != 2) {
                    buttonUpdateShovel.setEnabled(true);
                } else {
                    buttonUpdateShovel.setEnabled(false);
                }
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costUpdateGloves && levelUpdateGloves != 2) {
                    buttonUpdateGloves.setEnabled(true);
                } else {
                    buttonUpdateGloves.setEnabled(false);
                }

                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costBuffBonfire && !isAHot) {
                    buttonBuffBonfire.setEnabled(true);
                } else {
                    buttonBuffBonfire.setEnabled(false);
                }

                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costBuffVodka && countVodka != LIMIT_TO_VODKA) {
                    buttonBuffVodka.setEnabled(true);
                } else {
                    buttonBuffVodka.setEnabled(false);
                }
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= COST_WORKER && countOfWorkers != LIMIT_OF_WORKERS && levelUpdateShovel == 2) {
                    buttonBuyWorker.setEnabled(true);
                } else {
                    buttonBuyWorker.setEnabled(false);
                }
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= COST_OF_CALL_FOREMAN && countOfWorkers > 1 && foremanBuff != 1) {
                    buttonBuffForeman.setEnabled(true);
                } else {
                    buttonBuffForeman.setEnabled(false);
                }
                if(countVodka==LIMIT_TO_VODKA){
                    txtCostOfVodka.setVisibility(View.INVISIBLE);
                }
                if(countOfWorkers==5){
                    txtCostOfWorker.setVisibility(View.INVISIBLE);
                }
                String valueMetr1 = txtCountMetrs.getText().toString();
                double dValueMetr1 = Double.parseDouble(valueMetr1);

                if (dValueMetr1 > 100) {
                    chronometerAll.stop();
                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("YOU WON")
                            .setMessage("YOUR TIME:" + chronometerAll.getText())
                            .setCancelable(false)
                            .setNegativeButton("NICE",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(".MainActivity");
                                            startActivity(intent);
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                }

                String valueMoney = txtCountMoney.getText().toString();
                double dValueMoney = Double.parseDouble(valueMoney);
                dValueMoney = dValueMoney + (trueWorker * gloves * buffFromBonfie * buffVodka + countOfWorkers * SALLARY_WORKER_PER_SECOND + WORKER_DO_PER_SECOND * foremanBuff * countOfWorkers * gloves * buffFromBonfie * buffVodka) * bonfireIsOnFire;
                String strMoney = String.format("%.2f", dValueMoney);
                txtCountMoney.setText(strMoney);

                String valueMetr = txtCountMetrs.getText().toString();
                double dValueMetr = Double.parseDouble(valueMetr);
                dValueMetr = dValueMetr + (trueWorker * gloves * buffFromBonfie * buffVodka + countOfWorkers * (SALLARY_WORKER_PER_SECOND + WORKER_DO_PER_SECOND * gloves * buffFromBonfie * buffVodka) * foremanBuff) * bonfireIsOnFire;
                txtCountMetrs.setText(valueOf(dValueMetr));

                if (hotMetrToDig < (trueWorker * gloves * buffFromBonfie * buffVodka * bonfireIsOnFire + countOfWorkers * (SALLARY_WORKER_PER_SECOND + WORKER_DO_PER_SECOND * gloves * buffFromBonfie * buffVodka) * foremanBuff) * bonfireIsOnFire && isAHot) {
                    isAHot = false;
                    buffFromBonfie = 1;
                    hotMetrToDig = 0;
                    buttonBuffBonfire.setEnabled(true);
                    textMetrBonfire.setVisibility(View.INVISIBLE);
                    txtCostOfBonfire.setVisibility(View.VISIBLE);
                } else if (hotMetrToDig > (trueWorker * gloves * buffFromBonfie * buffVodka * bonfireIsOnFire + countOfWorkers * (SALLARY_WORKER_PER_SECOND + WORKER_DO_PER_SECOND * gloves * buffFromBonfie * buffVodka) * foremanBuff) * bonfireIsOnFire && isAHot) {
                    textMetrBonfire.setVisibility(View.VISIBLE);
                    hotMetrToDig = hotMetrToDig - (trueWorker * gloves * buffFromBonfie * buffVodka * bonfireIsOnFire + countOfWorkers * (SALLARY_WORKER_PER_SECOND + WORKER_DO_PER_SECOND * gloves * buffFromBonfie * buffVodka) * foremanBuff) * bonfireIsOnFire;
                    textMetrBonfire.setText(valueOf(hotMetrToDig));
                }
                txtCostOfBonfire.setText(valueOf(costBuffBonfire));
                txtCostOfForeman.setText(valueOf(COST_OF_CALL_FOREMAN));
                txtCostOfGloves.setText(valueOf(costUpdateGloves));
                txtCostOfShovel.setText(valueOf(costUpdateShovel));
                txtCostOfVodka.setText(valueOf(costBuffVodka));
                txtCostOfWorker.setText(valueOf(COST_WORKER));
            }
        });
    }

    private void addListenerOnButton() {
        //score per click
        buttonClick = (Button) findViewById(R.id.button_click);
        buttonClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String valueMoney = txtCountMoney.getText().toString();
                double dValueMoney = Double.parseDouble(valueMoney);
                dValueMoney = dValueMoney + digClick * gloves * buffFromBonfie * buffVodka;
                String strMoney = String.format("%.2f", dValueMoney);
                txtCountMoney.setText(strMoney);

                if (hotMetrToDig < digClick * gloves * buffFromBonfie * buffVodka && isAHot && bonfireIsOnFire == 1) {
                    isAHot = false;
                    buffFromBonfie = 1;
                    hotMetrToDig = 0;
                    buttonBuffBonfire.setEnabled(true);
                    textMetrBonfire.setVisibility(View.INVISIBLE);
                    txtCostOfBonfire.setVisibility(View.VISIBLE);
                } else if (hotMetrToDig > digClick * gloves * buffFromBonfie * buffVodka && isAHot && bonfireIsOnFire == 1) {
                    textMetrBonfire.setVisibility(View.VISIBLE);
                    hotMetrToDig = hotMetrToDig - digClick * gloves * buffFromBonfie * buffVodka;
                    textMetrBonfire.setText(valueOf(hotMetrToDig));

                }


                String valueMetr = txtCountMetrs.getText().toString();
                double dValueMetr = Double.parseDouble(valueMetr);
                dValueMetr = dValueMetr + digClick * gloves * buffFromBonfie * buffVodka;
                txtCountMetrs.setText(valueOf(dValueMetr));
            }
        });

        //UpdateShovel
        buttonUpdateShovel = (Button) findViewById(R.id.button_update_shovel);
        buttonUpdateShovel.setEnabled(false);
        buttonUpdateShovel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costUpdateShovel && levelUpdateShovel != 2) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("SHOVEL UPDATE TO " + (levelUpdateShovel + 1) + " LEVEL ")
                            .setMessage("DIGCLICK X3 lvl 1\nDIGCLICK X5 lvl 2\n" +
                                    "2 UPDATE WILL OPEN WORKER")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - costUpdateShovel;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);

                                    if (levelUpdateShovel == 0) {
                                        digClick = UPDATE_SHOVEL_1;
                                        levelUpdateShovel++;
                                        costUpdateShovel = costUpdateShovel * 3;
                                        buttonUpdateShovel.setEnabled(false);
                                    } else if (levelUpdateShovel == 1) {
                                        digClick = UPDATE_SHOVEL_2;
                                        levelUpdateShovel++;
                                        buttonUpdateShovel.setEnabled(false);
                                        txtCostOfShovel.setVisibility(View.INVISIBLE);
                                    }
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        //UpdateGloves
        buttonUpdateGloves = (Button) findViewById(R.id.button_update_gloves);
        buttonUpdateGloves.setEnabled(false);
        buttonUpdateGloves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costUpdateGloves && levelUpdateGloves != 2) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("GLOVES UPDATE TO " + (levelUpdateGloves + 1) + " LEVEL")
                            .setMessage("DIGCLICK X2 lvl 1\nDIGCLICK X5 lvl 2")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - costUpdateGloves;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);

                                    if (levelUpdateGloves == 0) {
                                        gloves = UPDATE_GLOVES_1;
                                        levelUpdateGloves++;
                                        costUpdateGloves = costUpdateGloves * 5;
                                        buttonUpdateGloves.setEnabled(false);
                                    } else if (levelUpdateGloves == 1) {
                                        gloves = UPDATE_GLOVES_2;
                                        levelUpdateGloves++;
                                        buttonUpdateGloves.setEnabled(false);
                                        txtCostOfGloves.setVisibility(View.INVISIBLE);
                                    }
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        //BuffBonfire
        buttonBuffBonfire = (Button) findViewById(R.id.button_buff_bonfire);
        buttonBuffBonfire.setEnabled(false);
        buttonBuffBonfire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costBuffBonfire && !isAHot) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("BONFIRE BUFF")
                            .setMessage("Wait 60 sek to heat the groud and take 20 hot-metrs to 2x dig\n" +
                                    "NO ONE CAN DIG")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - costBuffBonfire;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);
                                    textTimerBonfire.setVisibility(View.VISIBLE);
                                    txtCostOfBonfire.setVisibility(View.INVISIBLE);
                                    buffFromBonfie = 2;
                                    bonfireIsOnFire = 0;
                                    timeOfBonfireLeft = 60000;
                                    buttonClick.setEnabled(false);
                                    buttonBuffBonfire.setEnabled(false);
                                    isAHot = true;
                                    timeOfBonfire = new CountDownTimer(timeOfBonfireLeft, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            timeOfBonfireLeft = millisUntilFinished;
                                            updateTimerForBuff(timeOfBonfireLeft, textTimerBonfire);
                                        }

                                        @Override
                                        public void onFinish() {
                                            buttonClick.setEnabled(true);
                                            hotMetrToDig = 20;
                                            textTimerBonfire.setVisibility(View.INVISIBLE);
                                            textMetrBonfire.setVisibility(View.VISIBLE);
                                            bonfireIsOnFire = 1;
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        //BuffVodka
        buttonBuffVodka = (Button) findViewById(R.id.button_buff_vodka);
        buttonBuffVodka.setEnabled(false);
        buttonBuffVodka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= costBuffVodka && countVodka != LIMIT_TO_VODKA) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("VODKA, IN STOCK: " + countVodka)
                            .setMessage("give BUFFDRUNK 30% to dig, 60 sec\n" +
                                    "then DEBUFF HANGOVER -10% to dig, 30 sec\n" +
                                    "use 1 more in hangover will give you 90%, 120 sec\n" +
                                    "DONT USE WHEN DRUNK!!!")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - costBuffVodka;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);
                                    textTimerVodka.setVisibility(View.VISIBLE);
                                    if (!isDrunk && !isHangover) {
                                        isDrunk = true;
                                        buffVodka = BUFF_FROM_VODKA_LOW;
                                        countVodka++;
                                        timeOfVodkaLeft = TIME_OF_DRUNK_LEFT_1;
                                        buttonBuffVodka.setEnabled(false);
                                        timeOfDrunk = new CountDownTimer(timeOfVodkaLeft, 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                timeOfVodkaLeft = millisUntilFinished;
                                                updateTimerForBuff(timeOfVodkaLeft, textTimerVodka);
                                            }
                                            @Override
                                            public void onFinish() {
                                                isHangover = true;
                                                buffVodka = DEBUFF_FROM_HANGOVER_LOW;
                                                timeOfVodkaLeft = TIME_OF_HANGOVER_LEFT_1;
                                                timeOfHangover = new CountDownTimer(timeOfVodkaLeft, 1000) {
                                                    @Override
                                                    public void onTick(long millisUntilFinished) {
                                                        timeOfVodkaLeft = millisUntilFinished;
                                                        updateTimerForBuff(timeOfVodkaLeft, textTimerVodka);
                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        textTimerVodka.setVisibility(View.INVISIBLE);
                                                        buffVodka = 1;
                                                        isHangover = false;
                                                        isDrunk = false;
                                                    }
                                                }.start();
                                            }
                                        }.start();
                                    } else if (isDrunk && isHangover) {
                                        buffVodka = BUFF_FROM_VODKA_MAX;
                                        timeOfVodkaLeft = TIME_OF_DRUNK_LEFT_2;
                                        buttonBuffVodka.setEnabled(false);
                                        countVodka++;
                                        timeOfHangover.cancel();
                                        timeOfDrunk = new CountDownTimer(timeOfVodkaLeft, 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                timeOfVodkaLeft = millisUntilFinished;
                                                updateTimerForBuff(timeOfVodkaLeft, textTimerVodka);
                                            }
                                            @Override
                                            public void onFinish() {
                                                buffVodka = DEBUFF_FROM_HANGOWER_MAX;
                                                timeOfVodkaLeft = TIME_OF_DRUNK_HANGOVER_2;
                                                timeOfHangover = new CountDownTimer(timeOfVodkaLeft, 1000) {
                                                    @Override
                                                    public void onTick(long millisUntilFinished) {
                                                        timeOfVodkaLeft = millisUntilFinished;
                                                        updateTimerForBuff(timeOfVodkaLeft, textTimerVodka);
                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        buffVodka = 1;
                                                        textTimerVodka.setVisibility(View.INVISIBLE);
                                                        isDrunk = false;
                                                        isHangover = false;
                                                    }
                                                }.start();
                                            }
                                        }.start();
                                    } else if (isDrunk) {
                                        buffVodka = DEBUFF_LIKE_DEAD;
                                        countVodka++;
                                        timeOfVodkaLeft = TIME_OF_LIKE_DEAD;
                                        timeOfDrunk.cancel();
                                        timeOfHangover = new CountDownTimer(timeOfVodkaLeft, 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                timeOfVodkaLeft = millisUntilFinished;
                                                updateTimerForBuff(timeOfVodkaLeft, textTimerVodka);
                                            }

                                            @Override
                                            public void onFinish() {
                                                buffVodka = 1;
                                                textTimerVodka.setVisibility(View.INVISIBLE);
                                            }
                                        }.start();
                                    }
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        //Buy Worker
        buttonBuyWorker = (Button) findViewById(R.id.button_buy_workers);
        buttonBuyWorker.setEnabled(false);
        buttonBuyWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= COST_WORKER && countOfWorkers != LIMIT_OF_WORKERS && levelUpdateShovel == 2) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("BUY WORKER, YOU HAVE:" + countOfWorkers)
                            .setMessage("Random buy from 1-5 worker, only 1 will work\n" +
                                    "SALARY 5 cent per sec, dig 25 metr per sek\n" +
                                    "2 WORKERS WILL OPEN FOREMAN!")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - COST_WORKER;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);

                                    countOfWorkers++;
                                    Random rand = new Random();
                                    String worker = workers.get(rand.nextInt(workers.size()));
                                    workers.remove(worker);
                                    if (worker == "1") {
                                        trueWorker = 0.25;
                                    }
                                    buttonBuyWorker.setEnabled(false);
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        //Call Foreman
        buttonBuffForeman = (Button) findViewById(R.id.button_buff_foreman);
        buttonBuffForeman.setEnabled(false);
        buttonBuffForeman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Double.parseDouble(txtCountMoney.getText().toString()) >= COST_OF_CALL_FOREMAN && countOfWorkers > 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("CALL FOREMAN")
                            .setMessage("ALL OF WORKERS WILL WORK FOR 90 sek")
                            .setCancelable(false)
                            .setPositiveButton("BUY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String valueMoney = txtCountMoney.getText().toString();
                                    double dValueMoney = Double.parseDouble(valueMoney);
                                    dValueMoney = dValueMoney - COST_OF_CALL_FOREMAN;
                                    String strMoney = String.format("%.2f", dValueMoney);
                                    txtCountMoney.setText(strMoney);
                                    textTimerForeman.setVisibility(View.VISIBLE);
                                    foremanBuff = 1;
                                    timeOfFaremanLeft = 90000;
                                    buttonBuffForeman.setEnabled(false);
                                    txtCostOfForeman.setVisibility(View.INVISIBLE);
                                    timeOfWorkForeman = new CountDownTimer(timeOfFaremanLeft, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {
                                            timeOfFaremanLeft = millisUntilFinished;
                                            updateTimerForBuff(timeOfFaremanLeft, textTimerForeman);
                                        }
                                        @Override
                                        public void onFinish() {
                                            buttonBuffForeman.setEnabled(true);
                                            textTimerForeman.setVisibility(View.INVISIBLE);
                                            foremanBuff = 0;
                                            txtCostOfForeman.setVisibility(View.VISIBLE);
                                        }
                                    }.start();
                                }
                            })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
    }
    public void updateTimerForBuff(long timeOfBuff, TextView textOfBuff) {
        int minutes = (int) timeOfBuff / 60000;
        int secunds = (int) timeOfBuff % 60000 / 1000;
        String timeOfBonfire;

        timeOfBonfire = "" + minutes;
        timeOfBonfire += ":";
        if (secunds < 10) timeOfBonfire += "0";
        timeOfBonfire += secunds;
        textOfBuff.setText(timeOfBonfire);
    }
}
