package com.example.antonmedvediev.clickerrussia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

public class MainActivity extends AppCompatActivity {
    private Button toActivityGame, toExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerOnButton();
    }

    public void addListenerOnButton() {
        toActivityGame = (Button) findViewById(R.id.button_to_newGame);
        toActivityGame.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".GameActivity");
                        startActivity(intent);
                    }
                }
        );
        toExit = (Button) findViewById(R.id.button_to_exit);
        toExit.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          finish();
                                          System.exit(0);
                                      }
                                  }
        );
    }

}
